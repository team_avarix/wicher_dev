<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {

	$a_tasks = DB::table('tasks')->get();
	
	// return $a_tasks;
	
    return view('tasks', compact ('a_tasks'));
});
